# Bureau

Ce dépôt contient des documents utiles au fonctionnement de l'association :

- les [statuts d'origine](statuts_grimpo6.pdf) - 2017 - signés par Éric et Adrien
- une [version texte des statuts](statuts_grimpo6.md)
- une [version ODT (libreOffice) des statuts](status_grimpo6.odt)
- une [version ODT comprenant des révisions](status_grimpo6_revuBP.odt) destinées à alléger la rédaction et déporter certaines mentions dans le règlement intérieur 

Des documents reslatifs au fonctionnement en 2021 : 

- le [compte-rendu d'AG de mars 2021](CR_AG_Grimpo6_2021.pdf) qui sert de rapport d'activité ET liste les membres élus du bureau
- un document modifiable [listant les membres du bureau](grimpo6_bureau_2021.odt) à fournir aux administrations par exemple

Enfin, [le texte des statut de Cordée13](statuts_cordee13.md) (mars 2021), et un [document de questions](questionnaire-prises-decisions.pdf) rédigé par Grimpe13.