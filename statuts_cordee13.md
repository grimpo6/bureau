# Statuts de l’association Cordée 13


## Article 1 : Constitution, dénomination

Il est fondé entre les adhérents‚ aux présents statuts, une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour titre : « Cordée 13 ».

## Article 2 : Objets 

L'association a pour objets :

- l'organisation, le développement, la promotion de l’escalade, des activités de montagne et des activités physiques, sportives et de pleine nature voisines ou qui s’y rattachent ;
- la contribution à l'animation sportive, culturelle et sociale du quartier environnant le gymnase Glacière ;

Ses moyens d'action sont :

- l’organisation d’activités physiques et sportives ;
- l'organisation de manifestations et d'animations sportives ;
- la recherche des moyens de toute nature pour développer les activités physiques et sportives ;
- la coopération avec toutes structures, groupements, associations poursuivant des objectifs convergents.

## Article 3 : Siège social

Le siège social est fixé à Paris. 

## Article 4 : Durée

La durée de l'association est illimitée.

## Article 5 : Composition

L'association se compose de membres actifs et de membres d'honneur.

- Les membres actifs, qui paient une cotisation annuelle, participent régulièrement aux activités et contribuent à la réalisation des objectifs.
- Les membres d'honneur peuvent être désignés par l’assemblée générale.
L'association peut admettre des personnes morales.

## Article 6 : Cotisations

L’assemblée générale, sur proposition du bureau, fixe le montant de la cotisation.

## Article 7 : Adhésion

L’adhésion couvre une période d’un an, de septembre à septembre.
Le bureau peut refuser toute demande d'adhésion.

## Article 8 : Perte de la qualité de membre. 

La qualité de membre se perd par : décès, démission, pour non-paiement de la cotisation, par exclusion prononcée par le bureau pour infraction aux présents statuts ou au règlement intérieur.

## Article 9 : Le bureau et le Collectif d’animation

### 9.1 : le bureau

Le bureau est élu par l’assemblée générale annuelle. Sa composition peut être modifiée par l’assemblée générale. Il comprend au minimum un/e président/e et un/e trésorier/e. 

Les fonctions de membre du bureau ne peuvent être rémunérées. Seuls les frais et débours occasionnés par l'accomplissement de leur mandat leur sont remboursés au vu des pièces justificatives.

Le bureau est investi d'une manière générale des pouvoirs les plus étendus dans la limite des buts de l'association et dans le cadre des résolutions adoptées par les assemblées générales. Il peut autoriser tous actes et opérations permis à l'association et qui ne sont pas réservés aux assemblées générales.

La présence du quorum est nécessaire pour que le bureau puisse délibérer valablement. Seuls les membres présents peuvent participer aux votes. Les votes du bureau ont lieu à main levée.
Les membres sortants sont rééligibles. 

### 9.2 : le collectif d’animation 

Le collectif est une structure souple qui permet aux adhérent(e)s de s’investir, en fonction de leurs possibilités, dans la mise en œuvre des projets décidés en assemblée générale. 

## Article 10 : Assemblées générales

Les assemblées générales sont convoquées par le bureau au moins 15 jours avant la date de la réunion.

Elles se composent :

- des membres actifs à jour de leur cotisation.
- des éventuels membres d’honneur

Les décisions des assemblées générales sont prises à la majorité simple des membres présents. Le vote par procuration n'est pas autorisé. Toutes les délibérations sont prises à main levée. Toutefois un vote à bulletin secret peut-être émis à la demande d'un nombre de membres présents défini par le règlement intérieur.

### 10.1 : L'assemblée générale ordinaire

L'assemblée générale ordinaire, qui se réunit au moins une fois par an, entend le rapport d’activités et le rapport financier. Elle approuve les comptes de l'exercice clos, le budget pour l'exercice suivant et délibère sur toutes les autres questions figurant à l'ordre du jour. Elle élit les membres du bureau.

### 10.2 : L'assemblée générale extraordinaire

Pour la validité des décisions, l'assemblée générale extraordinaire doit comprendre au moins la moitié plus un des membres. Si cette proportion n'est pas atteinte, l'assemblée générale est convoquée de nouveau, à 15 jours d'intervalle. Elle peut alors délibérer quel que soit le nombre de membres présents.
L'assemblée générale extraordinaire statue sur les questions qui sont de sa seule compétence, à savoir les modifications à apporter aux présents statuts et la dissolution de l’association. Les délibérations sont prises obligatoirement à la majorité des deux tiers des membres présents.

## Article 11 : Ressources

Les ressources de l'association se composent :

- du produit des cotisations ;
- des subventions de l'État, des régions, des départements, des communes, des établissements publics ;
- du produit des fêtes et manifestations, des intérêts et redevances des biens et valeurs qu'elle pourrait posséder ainsi que des rétributions pour services rendus ;
- de toutes ressources ou subventions qui ne seraient pas contraires aux lois en vigueur.

## Article 12 : Comptabilité

Il est tenu à jour une comptabilité en recettes et en dépenses pour l'enregistrement de toutes les opérations financières. Il sera établi un bilan annuel. Le détail des comptes est accessible à tout membre sur demande. Chaque année deux membres sont désignés par l’AG pour examiner et valider les comptes de l’année en cours quand ceux-ci seront clos. Ces deux membres présenteront leur rapport lors de l’AG qui votera ces comptes.


## Article 13 : Dissolution

La dissolution est prononcée à la demande du bureau, par une assemblée générale convoquée spécialement à cet effet. Les conditions de convocation et de tenue d'une telle Assemblée sont celles prévues à l'Article 10.2 des présents statuts.

## Article 14 : Dévolution des biens

En cas de dissolution, l'assemblée générale extraordinaire désigne un ou plusieurs liquidateurs qui seront chargés de la liquidation des biens de l'association et dont elle détermine les pouvoirs. En aucun cas les membres de l'association ne peuvent se voir attribuer, en dehors de la reprise de leurs apports, une part quelconque des biens de l'association. L'actif net subsistant sera attribué obligatoirement à une ou plusieurs autres associations poursuivant des buts similaires et qui seront nommément désignées par l'assemblée générale extraordinaire.

## Article 15 : Règlement intérieur

Un règlement intérieur est établi par le bureau, qui le fait alors approuver par l'assemblée générale. Ce règlement éventuel est destiné à fixer les divers points non prévus par les présents statuts, notamment ceux ayant trait au fonctionnement pratique des activités de l'association.

Paris, le 

Signatures du président et du trésorier
