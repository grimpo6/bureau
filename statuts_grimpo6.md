# Statuts de l’association Grimpo6

## Article 1 : Constitution, dénomination

Il est fondé entre les adhérents‚ aux présents statuts, une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour titre : « Grimpo6 ».

## Article 2 : Objets

L'association a pour objets :

- l'organisation, le développement, la promotion de l’escalade, des activités de montagne et des activités physiques, sportives et de pleine nature voisines ou qui s’y rattachent ;
- la contribution à l'animation sportive et culturelle du 6ème arrondissement de Paris ;

Ses moyens d'action sont :

- l’organisation des activités physiques et sportives ;
- l'organisation de manifestations et d'animations sportives ;
- la recherche des moyens de toute nature pour développer les activités physiques et sportives ;
- la coopération avec toutes structures, groupements, associations poursuivant des objectifs convergents.

## Article 3 : Siège social

Le siège social est fixé à Paris. Le siège social peut être transféré sur décision du conseil d’administration.

## Article 4 : Durée

La durée de l'association est illimitée.

## Article 5 : Composition

L'association se compose de membres actifs et de membres d'honneur.

- les membres actifs, qui paient une cotisation annuelle, participent régulièrement aux activités et contribuent à la réalisation des objectifs.
- des membres d'honneur peuvent être désignés par l’assemblée générale.

L'association peut admettre des personnes morales.

## Article 6 : Cotisations

L’assemblée générale, sur proposition du conseil d’administration, fixe le montant de la cotisation qui inclut la licence et l’assurance FSGT omnisports.

## Article 7 : Adhésion

L'adhésion est réputée acquise dès la remise de la licence annuelle ou saisonnière qui constitue la carte d'adhérent. L’adhésion couvre une période d’un an, de septembre à septembre. Le conseil d’administration peut refuser toute demande d'adhésion.

## Article 8 : Perte de la qualité de membre

La qualité de membre se perd par : décès, démission, par radiation pour non-paiement de la cotisation, par exclusion prononcée par conseil d’administration pour infraction aux présents statuts ou motif grave portant préjudice moral ou matériel à l'association.

En cas de procédure d'exclusion, le membre concerné est invité, avant la prise de décision, à fournir des explications par lettre recommandée avec AR, devant le conseil d’administration.

## Article 9 : Responsabilité des membres

Aucun membre de l'association n'est personnellement responsable des engagements contractés par elle. Seul le patrimoine de l'association répond de ses engagements.

## Article 10 : Conseil d’administration et Collectif d’animation

### 10.1 : le conseil d’administration

Le conseil d’administration est élu par l’assemblée générale annuelle. Sa composition peut être modifiée par l’assemblée générale. Il comprend un/e président/e, un/e secrétaire, un/e trésorier/e. Et autant de vice-président, vice-trésorier, vice-secrétaire que souhaité.

Les fonctions de membre du conseil d’administration ne peuvent être rémunérées. Seuls les frais et débours occasionnés par l'accomplissement de leur mandat leur sont remboursés au vue des pièces justificatives.

Le conseil d’administration est investi d'une manière générale des pouvoirs les plus étendus dans la limite des buts de l'association et dans le cadre des résolutions adoptées par les assemblées générales. Il peut autoriser tous actes et opérations permis à l'association et qui ne sont pas réservés aux assemblées générales. La présence de la moitié au moins de ses membres est nécessaire pour que le conseil d’administration puisse délibérer valablement. Seuls les membres présents peuvent participer aux votes. Les votes du conseil d’administration ont lieu à main levée.

Les membres sortants sont rééligibles.

### 10.2 : le collectif d’animation

Le collectif est une structure souple qui permet aux adhérent(e)s de s’investir, en fonction de leurs possibilités, dans la mise en œuvre des projets décidés en assemblée générale. Le collectif se réunit à intervalle régulier, au moins une fois par trimestre.

## Article 11 : Assemblées générales

Les assemblées générales sont convoquées par le conseil d’administration au moins 15 jours avant la date de la réunion. Elles se composent :

- des membres actifs de plus de 16 ans à jour de leur cotisation.
- d’un représentant légal par adhérent de moins de 16 ans.
- des éventuels membres d’honneur

Les décisions des assemblées générales sont prises à la majorité des membres présents. Le vote par procuration n'est pas autorisé. Toutes les délibérations sont prises à main levée. Toutefois, à la demande du quart au moins des membres présents, les votes pourront être émis au scrutin secret.

### 11.1 : L'assemblée générale ordinaire

L'assemblée générale ordinaire, annuelle, entend les rapports sur la gestion du conseil d’administration notamment sur la situation morale et financière, le rapport financier du/de la trésorier/e. Elle approuve les comptes de l'exercice clos, le budget pour l'exercice suivant et délibère sur toutes les autres questions figurant à l'ordre du jour. Elle pourvoit l’élection des membres du conseil d’administration.

### 11.2 : L'assemblée générale extraordinaire

Pour la validité des décisions, l'assemblée générale extraordinaire doit comprendre au moins la moitié‚ plus un des membres ayant droit de vote. Si cette proportion n'est pas atteinte, l'assemblée générale est convoquée de nouveau, à 15 jours d'intervalle. Elle peut alors délibérer quel que soit le nombre de membres présents. L'assemblée générale extraordinaire statue sur les questions qui sont de sa seule compétence, à savoir les modifications à apporter aux présents statuts, la dissolution de l’association, etc. Les délibérations sont prises obligatoirement à la majorité des deux tiers des membres présents.

## Article 12 : Ressources.

Les ressources de l'association se composent :

- du produit des cotisations ;
- des subventions de l'Etat, des régions, des départements, des communes, des établissements publics ;
- du produit des fêtes et manifestations, des intérêts et redevances des biens et valeurs qu'elle pourrait posséder
ainsi que des rétributions pour services rendus ;
- toutes ressources ou subventions qui ne seraient pas contraires aux lois en vigueur.

## Article 13 : Comptabilité

Il est tenu à jour une comptabilité en recettes et en dépenses pour l'enregistrement de toutes les opérations financières. Cette comptabilité est tenue de préférence en partie double conformément au plan comptable général. Il sera établi annuellement un bilan et un compte de résultats.

## Article 14 : Dissolution

La dissolution est prononcée à la demande du conseil d’administration, par une assemblée générale convoquée spécialement à cet effet. Les conditions de convocation et de tenue d'une telle Assemblée sont celles prévues à l'Article 11.2 des présents statuts.

## Article 15 : Dévolution des biens

En cas de dissolution, l'assemblée générale extraordinaire désigne un ou plusieurs liquidateurs qui seront chargés de la liquidation des biens de l'association et dont elle détermine les pouvoirs. En aucun cas les membres de l'association ne peuvent se voir attribuer, en dehors de la reprise de leurs apports, une part quelconque des biens de l'association. L'actif net subsistant sera attribué obligatoirement à une ou plusieurs autres associations poursuivant des buts similaires et qui seront nommément désignées par l'assemblée générale extraordinaire.

## Article 16 : Règlement intérieur

Un règlement intérieur peut être établi par le conseil d’administration, qui le fait alors approuver par l'assemblée générale. Ce règlement éventuel est destiné à fixer les divers points non prévus par les présents statuts, notamment ceux ayant trait au fonctionnement pratique des activités de l'association.
